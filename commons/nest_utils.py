import nest


def create_detector(poplist):
    """
    Create a spike detector and attaches it to a list of populations
    """
    d = nest.Create('spike_detector', 1)
    for pop in poplist:
        nest.Connect(pop, d)
    return d
