from .datapath import datapath, healthy_rfa, healthy_cfa, stroke_rfa_acute, prepared_suffix

import glob
import os


class Trial:
    """
    Class representing a trial, with information on paths for both processed and unprocessed data
    """

    def __init__(self, folder, name):

        # data path
        self._name = name
        self._folder = folder

        # prepared path (file may not exist)
        self._prepared_name_base = name.split(".mat")[0]
        self._prepared_folder = folder + prepared_suffix

    @property
    def name(self):
        return self._name

    @property
    def folder(self):
        return self._folder

    @property
    def prepared_spikes(self):
        return self._prepared_name_base + "_spikes"

    @property
    def prepared_spikes_movements(self):
        return self._prepared_name_base + "_spikes_movements"

    @property
    def prepared_slidetimes(self):
        return self._prepared_name_base + "_slidetimes"

    @property
    def prepared_folder(self):
        return self._prepared_folder

    @property
    def name_full(self):
        return os.path.join(datapath, self._folder, self._name)

    @property
    def folder_full(self):
        return os.path.join(datapath, self._folder)

    @property
    def prepared_spikes_full(self):
        return os.path.join(datapath, self.prepared_folder, self.prepared_spikes)

    @property
    def prepared_spikes_movements_full(self):
        return os.path.join(datapath, self.prepared_folder, self.prepared_spikes_movements)

    @property
    def prepared_slidetimes_full(self):
        return os.path.join(datapath, self.prepared_folder, self.prepared_slidetimes)

    @property
    def prepared_folder_full(self):
        return os.path.join(datapath, self.prepared_folder)

    def is_prepared(self):
        ret = True
        ret = ret and os.path.exists(self.prepared_spikes_full)
        ret = ret and os.path.exists(self.prepared_spikes_movements_full)
        ret = ret and os.path.exists(self.prepared_slidetimes_full)
        return ret


# gather all trials from folders specified in the datapath module
all_healthy_cfa = [Trial(healthy_cfa, x.split('/')[-1]) for x in glob.glob(os.path.join(datapath, healthy_cfa, "*.mat"))]
all_healthy_rfa = [Trial(healthy_rfa, x.split('/')[-1]) for x in glob.glob(os.path.join(datapath, healthy_rfa, "*.mat"))]
all_stroke_rfa = [Trial(stroke_rfa_acute, x.split('/')[-1]) for x in glob.glob(os.path.join(datapath, stroke_rfa_acute, "*.mat"))]
