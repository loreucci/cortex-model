import time
import os
import nest
import scipy.io as sio
from nest_networks.spikes import import_spikes_from_file, scale_times, trim_to_resolution, get_gids, remap_gids, \
                                 get_spikes_from_detector, save_spikes_to_file
from nest_networks.recordingplayer import RecordingPlayer

from nest_networks.spinalcord import SpinalCord
from muscles_mouse import cord_flags, humerus1, humerus2, radius1, radius2, \
                g_humerus1, g_humerus2, g_radius1, g_radius2, \
                flex_extend_humerus, flex_extend_radius

import sys
sys.path.append("..")
from commons import all_stroke_rfa, datapath
from models.cortex import cfa


def simulate_motion(input_file, T, dt, start_cut, W):

    cortex_params = {

        'cfa_p_neurons': 636 + 1155 + 1155,
        'cfa_fs_neurons': 89 + 182 + 202 + 411,

        'p_p_synapses': 0.049,
        'p_fs_synapses': 0.065,
        'fs_fs_synapses': 0.204,
        'fs_p_synapses': 0.216,

        'w_p_p': W[0],
        'w_p_fs': W[1],
        'w_fs_fs': W[2],
        'w_fs_p': W[3],
        'sigma_w': 8.8,
        'g': -4.,

        'de': 1.5,
        'sigma_de': 0.75,
        'di': 0.8,
        'sigma_di': 0.4,

        'controlateral_rate': 3. * 7000,
    }
    cfa_neurons = cortex_params['cfa_p_neurons'] + cortex_params['cfa_fs_neurons']

    # simulation setup
    nest.ResetKernel()
    nest.SetKernelStatus({'resolution': dt, "local_num_threads": 4})  # set simulation resolution"

    # RFA
    spikes_mu_tocut = import_spikes_from_file(input_file.prepared_spikes_full, 1)
    spikes_mu = [(idx, time - start_cut) for idx, time in spikes_mu_tocut if time > start_cut]
    spikes_mu = scale_times(spikes_mu, 1000.0)
    spikes_mu = trim_to_resolution(spikes_mu)
    gids = get_gids(spikes_mu)
    spikes_mu = remap_gids(spikes_mu, orig=gids, dest=range(len(gids)))
    rfa = RecordingPlayer(spikes_mu, mult=int(cfa_neurons / 2.0) // 40, sigma=5.)

    # CFA
    cfa_p, cfa_fs = cfa(cortex_params)

    # RFA to CFA
    nest.Connect(rfa.get_gids(),
                 cfa_p[0:int(len(cfa_p) * 0.1)],
                 'all_to_all',
                 # {'rule': 'fixed_total_number', 'N': int(0.1*cfa_p_neurons*len(rfa.get_gids()))},
                 {'weight': cortex_params['w_p_p'], 'delay': 10.})

    # # leaky integrator interneurons
    # leaky_params = {'C_m': 250.0,
    #                 'E_L': -70.0,
    #                 'I_e': 0.0,
    #                 'V_m': -70.0,
    #                 'V_reset': -70.0,
    #                 'V_th': -55.0,
    #                 'beta_Ca': 0.001,
    #                 't_ref': 2.0,
    #                 'tau_Ca': 10000.0,
    #                 'tau_m': 10.0,  # <---
    #                 'tau_minus': 20.0,
    #                 'tau_minus_triplet': 110.0,
    #                 'tau_syn_ex': 2.0,
    #                 'tau_syn_in': 2.0,
    # }
    #
    # inter = nest.Create('iaf_psc_exp', 200, leaky_params)
    # nest.Connect(cfa_p[0:int(len(cfa_p)*0.1)], inter,
    #              {'rule': 'fixed_total_number', 'N': int(0.1*cortex_params['cfa_p_neurons']*200)},
    #              {'weight': cortex_params['w_p_p'] / 10.0, 'delay': 2.})

    # spinal cord
    sc = SpinalCord(cord_flags,
                    [humerus1, humerus2, radius1, radius2],
                    [g_humerus1, g_humerus2, g_radius1, g_radius2],
                    [flex_extend_humerus, flex_extend_radius])
    desc = {
        'humerus2': {'alpha': cfa_p[0:int(len(cfa_p) * 0.1)], 'alpha_syn_spec': {'weight': 2.0, 'delay': 1.0}},
        'radius1': {'alpha': cfa_p[0:int(len(cfa_p) * 0.1)], 'alpha_syn_spec': {'weight': 2.0, 'delay': 1.0}}
    }
    sc.attach_descending(desc)

    # spike detector
    sd = nest.Create('spike_detector', 1)
    nest.Connect(cfa_p, sd)
    nest.Connect(cfa_fs, sd)
    nest.Connect(rfa.get_gids(), sd)
    sd_sc = nest.Create('spike_detector', 1)
    nest.Connect(sc.get_all_recordable_gids(), sd_sc)

    # muscle activation recording
    network = sc.get_IO_populations()
    mul = nest.Create('multimeter', 1, params={'record_from': ['activation']})
    nest.Connect(mul, network['humerus2_m'])

    # simulation
    nest.Simulate(T)

    # get simulation results
    spikes = get_spikes_from_detector(sd)
    events = nest.GetStatus(mul)[0]["events"]
    m = network['humerus2_m']
    # t_activations = [x for (x, y) in zip(events["times"], events["senders"]) if y == m]
    activations = [x for (x, y) in zip(events["activation"], events["senders"]) if y == m]

    # save to files
    timestamp = time.strftime("%Y-%m-%d-%H%M%S")
    savefbase = os.path.join(datapath, 'Results', input_file.name + '_' + timestamp + '_')
    save_spikes_to_file(spikes, savefbase + 'spikes.txt')
    with open(savefbase + 'activations.txt', 'w') as actfile:
        actfile.write('activation\n')
        for _a in activations:
            actfile.write("%f\n" % _a)
    actfile.close()


if __name__ == '__main__':

    # simulation parameters
    dt = 0.1
    start_cut = 0
    input_files = all_stroke_rfa[6:7]

    # motor cortex parameters
    W = [32.577845583436286, 159.58162815402198, -333.34751589067685, -241.94810158140854]

    for i, file in enumerate(input_files):

        print("Simulating {}/{} ({})".format(i+1, len(input_files), file.name))

        # get simulation length
        matfile = sio.loadmat(file.name_full, variable_names=['time_sync'])
        T = matfile['time_sync'][-1][0]

        # simulation
        simulate_motion(file, int(T) * 1000.0, dt, start_cut, W)
