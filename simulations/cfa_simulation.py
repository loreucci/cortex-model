import nest
from nest import raster_plot

import sys
sys.path.append("..")
from models.cortex import cfa
from models.stroke import apply_stroke
from commons.nest_utils import create_detector


def print_rates(sd_p, cfa_p_n, sd_fs, cfa_fs_n, sd, sim_t):
    active_p = set(nest.GetStatus(sd_p, 'events')[0]['senders'])
    print("active p: {}".format(len(active_p)))
    active_fs = set(nest.GetStatus(sd_fs, 'events')[0]['senders'])
    print("active fs: {}".format(len(active_fs)))

    spikecount_p = nest.GetStatus(sd_p, 'n_events')[0]
    rate_p = spikecount_p / (sim_t / 1000.0) / cfa_p_n
    spikecount_fs = nest.GetStatus(sd_fs, 'n_events')[0]
    rate_fs = spikecount_fs / (sim_t / 1000.0) / cfa_fs_n
    rate_avg = nest.GetStatus(sd, 'n_events')[0] / (sim_t / 1000.0) / (cfa_p_n + cfa_fs_n)
    print("Rate: p = %.2f, fs = %.2f, avg = %.2f" % (rate_p, rate_fs, rate_avg))

    return rate_p, rate_fs, rate_avg


def cfa_simulation(sim_t, cortex_params=None, stroke=0.0, plot=True):
    """
    Simulate only CFA, for a certain amount of time. Returns the firing rates of the pyramidal and fast spiking
    populations.
    """
    # simulation parameters
    dt = 0.1

    # simulation setup
    nest.ResetKernel()
    nest.SetKernelStatus({'resolution': dt, "local_num_threads": 4})

    cfa_p, cfa_fs = cfa(cortex_params)
    cfa_p_n = len(cfa_p)
    cfa_fs_n = len(cfa_fs)

    # detectors
    sd = create_detector([cfa_p, cfa_fs])
    sd_p = create_detector([cfa_p])
    sd_fs = create_detector([cfa_fs])

    # remove transient
    nest.Simulate(10.0)
    nest.SetStatus(sd_p, {'n_events': 0})
    nest.SetStatus(sd_fs, {'n_events': 0})
    nest.SetStatus(sd, {'n_events': 0})

    # simulation
    nest.Simulate(sim_t)

    if stroke > 0.0:
        print("pre")
    rate_p, rate_fs, rate_avg = print_rates(sd_p, cfa_p_n,
                                            sd_fs, cfa_fs_n,
                                            sd, sim_t)

    # stroke
    s_rates = ()
    if stroke > 0.0:
        sd_st = create_detector([cfa_p, cfa_fs])
        sd_st_p = create_detector([cfa_p])
        sd_st_fs = create_detector([cfa_fs])

        apply_stroke(cfa_p, cfa_fs, stroke)
        nest.Simulate(sim_t)

        print("post")
        s_rates = print_rates(sd_st_p, cfa_p_n,
                              sd_st_fs, cfa_fs_n,
                              sd_st, sim_t)

    # plot spikes
    if plot:
        raster_plot.from_device(sd)
        raster_plot.show()

    if stroke > 0.0:
        return (rate_p, rate_fs, rate_avg) + s_rates
    return rate_p, rate_fs, rate_avg
