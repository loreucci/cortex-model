import cma
import nest
import pandas as pd
import argparse
import time

import sys
sys.path.append("..")
from models.cortex import MotorArea


parser = argparse.ArgumentParser("baseline parameters")
parser.add_argument('--averagerate', type=float, required=True)
parser.add_argument('--external', action="store_true")
parser.add_argument('--weights', type=float, nargs=4)
args = parser.parse_args()

external = False
if args.external:
    external = True
    if args.weights is None:
        print("Error: Weights must be provided when optimizing external inputs")
        exit(1)

# parameters
average_rate = args.averagerate  # Hz (11.8148)
fs_p_ratio = 2.4

if external:
    initial_solution = 5. * 7000
    initial_weights = args.weights
else:
    initial_solution = [20.0, 110.0, -5.0, -50.0]
    initial_weights = [None] * 4

maxiter = 100  # max number of iterations for CMA-ES

# data collector
if external:
    all_solutions = pd.DataFrame(columns=['ext', 'loss'])
else:
    all_solutions = pd.DataFrame(columns=['w_p_p', 'w_p_fs', 'w_fs_fs', 'w_fs_p', 'loss'])
all_solutions_id = 0

starting_cortex_params = {

    'cfa_p_neurons': 636+1155+1155,
    'cfa_fs_neurons': 89+182+202+411,

    'p_p_synapses': 0.049,
    'p_fs_synapses': 0.065,
    'fs_fs_synapses': 0.204,
    'fs_p_synapses': 0.216,

    'w_p_p': initial_weights[0],
    'w_p_fs': initial_weights[1],
    'w_fs_fs': initial_weights[2],
    'w_fs_p': initial_weights[3],
    'sigma_w': 8.8,
    'g': -4.,

    'de': 1.5,
    'sigma_de': 0.75,
    'di': 0.8,
    'sigma_di': 0.4,

    'external_rate': 5. * 5000,
    'controlateral_rate': 5. * 1000,
}

# compute desired rates
desired_p = average_rate * (starting_cortex_params['cfa_p_neurons']+starting_cortex_params['cfa_fs_neurons']) / (starting_cortex_params['cfa_p_neurons']+fs_p_ratio*starting_cortex_params['cfa_fs_neurons'])  # Hz
desired_fs = fs_p_ratio * desired_p  # Hz


def normalize_weigths(w):
    nw = [0.0] * 4
    nw[0] = w[0] / 200.0
    nw[1] = w[1] / 400.0
    nw[2] = -w[2] / 400.0
    nw[3] = -w[3] / 800.0
    return nw


def denormalize_weights(nw):
    w = [0.0] * 4
    w[0] = nw[0] * 200.0
    w[1] = nw[1] * 400.0
    w[2] = -nw[2] * 400.0
    w[3] = -nw[3] * 800.0
    return w


def normalize_external(e):
    return [e / 100000]


def denormalize_external(ne):
    return ne[0] * 100000


def compute_resting(pars):

    global all_solutions_id

    # set parameters for individual
    cortex_params = starting_cortex_params.copy()

    # parameters of the current solution
    er, w = None, None
    if external:
        er = denormalize_external(pars)
        cortex_params['controlateral_rate'] = er
    else:
        w = denormalize_weights(pars)
        cortex_params['w_p_p'] = w[0]
        cortex_params['w_p_fs'] = w[1]
        cortex_params['w_fs_fs'] = w[2]
        cortex_params['w_fs_p'] = w[3]

    nest.ResetKernel()
    nest.SetKernelStatus({'resolution': 0.1, "local_num_threads": 4})

    # create cortex network
    ma = MotorArea(cortex_params)
    cfa_p, cfa_fs = ma.ipsilateral
    sd_p = nest.Create('spike_detector', 1)
    nest.Connect(cfa_p, sd_p)
    sd_fs = nest.Create('spike_detector', 1)
    nest.Connect(cfa_fs, sd_fs)

    # simulation
    sim_t = 1000.0
    nest.Simulate(sim_t)

    # compute fitness from rate
    spikecount_p = nest.GetStatus(sd_p, 'n_events')[0]
    rate_p = spikecount_p / (sim_t / 1000.0) / cortex_params['cfa_p_neurons']
    spikecount_fs = nest.GetStatus(sd_fs, 'n_events')[0]
    rate_fs = spikecount_fs / (sim_t / 1000.0) / cortex_params['cfa_fs_neurons']

    loss = abs(rate_p - desired_p) + abs(rate_fs - desired_fs)

    # save current solution
    if external:
        all_solutions.loc[all_solutions_id] = [er, loss]
    else:
        all_solutions.loc[all_solutions_id] = w + [loss]
    all_solutions_id += 1

    return loss


nest.set_verbosity('M_WARNING')

opts = cma.CMAOptions({'maxiter': maxiter,
                       'bounds': [0.0, 1.0]})
if external:
    evo = cma.CMAEvolutionStrategy(normalize_external(initial_solution), 0.2, opts)
else:
    evo = cma.CMAEvolutionStrategy(normalize_weigths(initial_solution), 0.2, opts)
while not evo.stop():
    solutions = evo.ask()
    evals = [compute_resting(_pars) for _pars in solutions]
    evo.tell(solutions, evals)
    evo.logger.add()
    evo.disp()

res = evo.result
print("Optimization result: {}".format(denormalize_weights(res.xbest)))
cma.plot()

all_solutions.to_csv('allsolutions' + time.strftime("%Y-%m-%d-%H%M%S") + '.csv')
