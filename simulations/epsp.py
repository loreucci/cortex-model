import nest
import matplotlib.pyplot as plt
import numpy as np

import sys
sys.path.append("..")
from models.cortex import default_neuron_params_ex


# weights to be tested
test_weights = [87.8, 187.8, -87.8, -187.8]
N = len(test_weights)

# create test network
n = nest.Create('iaf_psc_exp', N, default_neuron_params_ex)
sg = nest.Create('spike_generator', 1, {'spike_times': [10.0]})
m = nest.Create('multimeter', params={'record_from': ['V_m']})
for i in range(N):
    nest.Connect(sg, [n[i]], 'all_to_all', {"weight": test_weights[i], "delay": 1.0})
nest.Connect(m, n)

# simulation
sim_time = 20.0
nest.Simulate(sim_time)

# get membrane potential events
events = nest.GetStatus(m)[0]['events']
t = [t for s, t in zip(events['senders'], events['times']) if s == 1]
vm = [[v for s, v in zip(events['senders'], events['V_m']) if s == i] for i in range(1, N+1)]

# plot membrane potential
ncols = int(np.ceil(np.sqrt(N)))
nrows = (N-1) // ncols + 1
fig, axes = plt.subplots(nrows, ncols, constrained_layout=True)
for i in range(N):
    axes.flat[i].plot(t, vm[i])
    axes.flat[i].axis([0, sim_time, -66, -64])
    axes.flat[i].set_ylabel('Membrane potential [mV]')
    axes.flat[i].set_xlabel('Time [ms]')
    axes.flat[i].set_title(str(test_weights[i]))

plt.show()
