import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import scipy.io as sio
import glob
import os

import sys
sys.path.append("..")
from commons import all_stroke_rfa, datapath


def plot_activations(ax, activations, reference, save_to_file=None):

    # activations
    act_t = np.array(range(len(activations['activation']))) / 1000.
    ax.plot(act_t, activations['activation'])

    # reference
    ax.plot(reference['time_sync'], reference['pos_sync'] / np.max(reference['pos_sync']))

    if save_to_file is not None:
        plt.savefig(save_to_file)


def plot_folder(results_folder, data_folder):
    results_files = glob.glob(os.path.join(datapath, results_folder, "*activations.txt"))

    for actfile in results_files:
        # get reference file
        reffile = actfile[:actfile.rfind('.mat')]
        reffile = reffile.replace(results_folder, data_folder) + '.mat'

        ref = sio.loadmat(reffile, variable_names=['time_sync', 'pos_sync'])
        ref['pos_sync'][np.isnan(ref['pos_sync'])] = 0.0

        # load activations data
        actdata = pd.read_csv(actfile)

        _, axis = plt.subplots()
        plot_activations(axis, actdata, ref, save_to_file=actfile + ".png")
        # plt.show()


plot_folder("Results", "Stroke-RFA-acute")
