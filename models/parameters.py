# neuron params from
# Khatoun, A., Asamoah, B., & Mc Laughlin, M. (2017). Simultaneously excitatory and inhibitory effects of transcranial
# alternating current stimulation revealed using selective pulse-train stimulation in the rat motor cortex.
# Journal of Neuroscience, 37(39), 9389-9402.
default_neuron_params_ex = {
    'C_m': 500.,  # pF
    'I_e': 0.0,  # nA
    'tau_m': 18.0,  # ms
    't_ref': 10.0,  # ms
    'tau_syn_ex': 1.2,  # ms
    'tau_syn_in': 1.2,  # ms
    'V_reset': -50.0,  # mV
    'E_L': -60.0,  # mV
    'V_th': -40.0,  # mV
    'V_m': -60.0  # mV
}

default_neuron_params_in = {
    'C_m': 500.,  # pF
    'I_e': 0.0,  # nA
    'tau_m': 9.0,  # ms
    't_ref': 3.0,  # ms
    'tau_syn_ex': 0.4,  # ms
    'tau_syn_in': 0.4,  # ms
    'V_reset': -50.0,  # mV
    'E_L': -60.0,  # mV
    'V_th': -40.0,  # mV
    'V_m': -60.0  # mV
}

cortex_optimized_params = {

    'cfa_p_neurons': 636 + 1155 + 1155,
    'cfa_fs_neurons': 89 + 182 + 202 + 411,

    'p_p_synapses': 0.049,
    'p_fs_synapses': 0.065,
    'fs_fs_synapses': 0.204,
    'fs_p_synapses': 0.216,

    'w_p_p': 63.96037367500755,
    'w_p_fs': 183.35388636338757,
    'w_fs_fs': -188.72452287469235,
    'w_fs_p': -422.5928602200412,
    'sigma_w': 8.8,
    'g': -4.,

    'de': 1.5,
    'sigma_de': 0.75,
    'di': 0.8,
    'sigma_di': 0.4,

    'external_rate': 5. * 5000,
    'controlateral_rate': 5. * 1000
}
