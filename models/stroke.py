import nest
import numpy as np


def get_stroke_order(pop):
    return np.random.permutation(pop)


def apply_stroke(cfa_p, cfa_fs, perc, stroke_order=None):

    if stroke_order is None:
        stroke_order = get_stroke_order(cfa_p + cfa_fs)

    n_to_kill = int(len(cfa_p + cfa_fs) * perc)
    to_kill = stroke_order[:n_to_kill]

    nest.SetStatus(to_kill.tolist(), {'frozen': True})
