import nest
import numpy as np

from .parameters import default_neuron_params_ex, default_neuron_params_in, cortex_optimized_params


def cfa(params=None, neuron_params_ex=None, neuron_params_in=None):
    """
    Creates a network of L5 of the motor cortex. Parameters can be supplied via a dictionary with the following values:

    cfa_p_neurons:      number of piramidal neurons
    cfa_fs_neurons:     number of fast spiking interneurons

    p_p_synapses:       probability of connections P-P
    p_fs_synapses:      probability of connections P-FS
    fs_fs_synapses:     probability of connections FS-FS
    fs_p_synapses:      probability of connections FS-P

    w_p_p:              mean excitatory P-P weight (pA)
    w_p_fs:             mean excitatory P-FS weight (pA)
    w_fs_fs:            mean excitatory FS-FS weight (pA)
    w_fs_p:             mean excitatory FS-P weight (pA)
    sigma_w:            standard deviation of excitatory weight (pA)
    g:                  relative inhibitory standard deviation gain

    de:                 mean spike transmission delay for excitatory presynaptic neurons (ms)
    sigma_de:           standard deviation
    di:                 mean spike transmission delay for inhibitory presynaptic neurons (ms)
    sigma_di:           standard deviation

    external_rate:      activity of other brain areas
    controlateral_rate: activity of controlateral stimulation
    """
    if params is None:
        params = cortex_optimized_params

    # get parameters from dictionary
    cfa_p_neurons = params['cfa_p_neurons']
    cfa_fs_neurons = params['cfa_fs_neurons']

    p_p_synapses = params['p_p_synapses']
    p_fs_synapses = params['p_fs_synapses']
    fs_fs_synapses = params['fs_fs_synapses']
    fs_p_synapses = params['fs_p_synapses']

    w_p_p = params['w_p_p']
    w_p_fs = params['w_p_fs']
    w_fs_fs = params['w_fs_fs']
    w_fs_p = params['w_fs_p']
    sigma_w = params['sigma_w']
    g = params['g']
    de = params['de']
    sigma_de = params['sigma_de']
    di = params['di']
    sigma_di = params['sigma_di']

    external_rate = params['external_rate']
    # controlateral_rate = params['controlateral_rate']

    # default params
    # need to do this to avoid having mutable default parameters
    if neuron_params_ex is None:
        neuron_params_ex = default_neuron_params_ex
    if neuron_params_in is None:
        neuron_params_in = default_neuron_params_in

    # synapses parameters
    p_p_syn_dict = {
        'weight': {
            'distribution': 'normal_clipped',
            'mu': w_p_p,
            'sigma': sigma_w,
            'low': 0.0
        },
        'delay': {
            'distribution': 'normal_clipped',
            'mu': de,
            'sigma': sigma_de,
            'low': 0.1
        }
    }

    p_fs_syn_dict = {
        'weight': {
            'distribution': 'normal_clipped',
            'mu': w_p_fs,
            'sigma': sigma_w,
            'low': 0.0
        },
        'delay': {
            'distribution': 'normal_clipped',
            'mu': de,
            'sigma': sigma_de,
            'low': 0.1
        }
    }

    fs_fs_syn_dict = {
        'weight': {
            'distribution': 'normal_clipped',
            'mu': w_fs_fs,
            'sigma': np.abs(sigma_w * g),
            'high': 0.0
        },
        'delay': {
            'distribution': 'normal_clipped',
            'mu': di,
            'sigma': sigma_di,
            'low': 0.1
        }
    }

    fs_p_syn_dict = {
        'weight': {
            'distribution': 'normal_clipped',
            'mu': w_fs_p,
            'sigma': np.abs(sigma_w * g),
            'high': 0.0
        },
        'delay': {
            'distribution': 'normal_clipped',
            'mu': di,
            'sigma': sigma_di,
            'low': 0.1
        }
    }

    # create network
    cfa_p = nest.Create('iaf_psc_exp', cfa_p_neurons, neuron_params_ex)
    cfa_fs = nest.Create('iaf_psc_exp', cfa_fs_neurons, neuron_params_in)

    nest.Connect(cfa_p,
                 cfa_p,
                 {'rule': 'fixed_total_number', 'N': int(cfa_p_neurons * cfa_fs_neurons * p_p_synapses)},
                 p_p_syn_dict)

    nest.Connect(cfa_p,
                 cfa_fs,
                 {'rule': 'fixed_total_number', 'N': int(cfa_p_neurons * cfa_fs_neurons * p_fs_synapses)},
                 p_fs_syn_dict)

    nest.Connect(cfa_fs,
                 cfa_fs,
                 {'rule': 'fixed_total_number', 'N': int(cfa_p_neurons * cfa_fs_neurons * fs_fs_synapses)},
                 fs_fs_syn_dict)

    nest.Connect(cfa_fs,
                 cfa_p,
                 {'rule': 'fixed_total_number', 'N': int(cfa_p_neurons * cfa_fs_neurons * fs_p_synapses)},
                 fs_p_syn_dict)

    # external and controlateral
    ext = nest.Create('poisson_generator', params={'rate': external_rate})
    # cl = nest.Create('poisson_generator', params={'rate': controlateral_rate})
    nest.Connect(ext, cfa_p, 'all_to_all', {'weight': w_p_p, 'delay': de})
    # nest.Connect(cl, cfa_p, 'all_to_all', {'weight': w_p_p, 'delay': de})  # check this 87.8

    nest.Connect(ext, cfa_fs, 'all_to_all', {'weight': w_p_fs, 'delay': de})

    return cfa_p, cfa_fs


class MotorArea:

    def __init__(self, cortex_params=None, neuron_params_ex=None, neuron_params_in=None):

        if cortex_params is None:
            cortex_params = cortex_optimized_params

        # create both cortices
        self._ipsilateral = cfa(cortex_params, neuron_params_ex, neuron_params_in)

        self._controlateral = cfa(cortex_params, neuron_params_ex, neuron_params_in)

        # connect the two
        w_p_p = params['w_p_p']
        de = params['de']
        nest.Connect(self.controlateral[0], self.ipsilateral[0], {'weight': w_p_p, 'delay': de})
        nest.Connect(self.ipsilateral[0], self.controlateral[0],
                     {'rule': 'pairwise_bernoulli', 'p': 0.1},
                     {'weight': w_p_p, 'delay': de})

    @property
    def ipsilateral(self):
        return self._ipsilateral

    @property
    def controlateral(self):
        return self._controlateral
