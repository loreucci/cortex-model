function [baseline_mean, baseline_stddev, baseline_median, rates] = find_baseline(filename, lowchannel, interval)

% load file, with only the variables needed
load(filename, 'time_sync', 'index_baseline', '-regexp', '^ts\d*');

% resting times (middle of intervals)
baseline_times = time_sync(index_baseline);

% camputer total number of spikes in resting intervals
spikenum = zeros(length(baseline_times), 1);
for ch=lowchannel:16
    spikes = eval(strcat('ts', num2str(ch)));
    for sp=1:length(spikes)
        for t=1:length(baseline_times)
            if spikes(sp) > baseline_times(t)-interval/2 && spikes(sp) < baseline_times(t)+interval/2
                spikenum(t) = spikenum(t) + 1;
            end
        end
    end
end

% average baseline per channel
rates = spikenum / (16-lowchannel+1) / interval;
baseline_mean = mean(rates);
baseline_stddev = std(rates);
baseline_median = median(rates);