function [active_mean, active_stddev, active_median, rates] = find_active(filename, lowchannel, interval)

% load file, with only the variables needed
load(filename, 'time_sync', 'index_mov', '-regexp', '^ts\d*');

% resting times (middle of intervals)
active_times = time_sync(index_mov);

% camputer total number of spikes in resting intervals
spikenum = zeros(length(active_times), 1);
for ch=lowchannel:16
    spikes = eval(strcat('ts', num2str(ch)));
    for sp=1:length(spikes)
        for t=1:length(active_times)
            if spikes(sp) > active_times(t)-interval/2 && spikes(sp) < active_times(t)+interval/2
                spikenum(t) = spikenum(t) + 1;
            end
        end
    end
end

% average baseline per channel
rates = spikenum / (16-lowchannel+1) / interval;
active_mean = mean(rates);
active_stddev = std(rates);
active_median = median(rates);