clear; close('all');

datafolder = fileread('datapath.txt');

filenames = {'spk_mulitunits_topo01_day00_File1_CFA.mat', ...
             'spk_mulitunits_topo01_day00_File2_CFA.mat', ...
             'spk_mulitunits_topo01_day01_File3_CFA.mat', ...
             'spk_mulitunits_topo01_day01_File4_CFA.mat', ...
             'spk_mulitunits_topo02_day00_File1_CFA.mat', ...
             'spk_mulitunits_topo02_day00_File2_CFA.mat', ...
             'spk_mulitunits_topo02_day01_File3_CFA.mat', ...
             'spk_mulitunits_topo02_day01_File4_CFA.mat'};


baseline_medians = zeros((length(filenames)),1);
         
figure;
f1 = subplot(2, 1, 1);
f2 = subplot(2, 1, 2);
for fid = 1:length(filenames)
    fname = strcat(datafolder, '/Healthy-CFA/', filenames{fid});
    [baseline_mean, baseline_stddev, baseline_median, baseline_rates] = find_baseline(fname, 8, 1.0);
    [active_mean, active_stddev, active_median, active_rates] = find_active(fname, 8, 0.5);
    fprintf("%s, baseline: mean=%f, std=%f, median=%f, active: mean=%f, std=%f, median=%f\n", filenames{fid}, baseline_mean, baseline_stddev, baseline_median, active_mean, active_stddev, active_median);
    plot(f1, baseline_rates, 'LineStyle', '-', 'Marker', '*', 'DisplayName', replace(filenames{fid}, '_', '-'));
    plot(f2, active_rates, 'LineStyle', '-', 'Marker', '*', 'DisplayName', replace(filenames{fid}, '_', '-'));
    hold(f1, 'all');
    hold(f2, 'all');
    baseline_medians(fid) = baseline_median / 3;
end
ylim(f1, [0 220]);
ylim(f2, [0 220]);
legend(f1);
legend(f2);

baseline_medians