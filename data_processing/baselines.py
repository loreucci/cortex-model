import scipy.io as sio
import os
import numpy as np

import sys
sys.path.append("..")
import commons


def find_baseline(filename, lowchannel, interval):

    # load files
    channels = ['ts' + str(i) for i in range(lowchannel, 16 + 1)]
    file = sio.loadmat(filename, variable_names=['time_sync', 'index_baseline'] + channels)

    # get times of baselines
    baseline_times = file['time_sync'][file['index_baseline']-1].reshape(-1)
    restings = len(baseline_times)

    # compute total number of spikes in resting intervals
    spikenum = np.zeros(restings)
    for ch in channels:
        spikes = file[ch]
        for sp in spikes:
            for t in range(restings):
                if baseline_times[t] - interval/2.0 < sp[0] < baseline_times[t] + interval/2.0:
                    spikenum[t] += 1

    # average baseline per channel
    rates = spikenum / (16 - lowchannel + 1) / interval

    baseline_mean = np.mean(rates)
    baseline_stddev = np.std(rates)
    baseline_median = np.median(rates)
    return baseline_mean, baseline_stddev, baseline_median, rates


def find_active(filename, lowchannel, interval):

    # load mat file
    channels = ['ts' + str(i) for i in range(lowchannel, 16 + 1)]
    file = sio.loadmat(filename, variable_names=['time_sync', 'index_mov'] + channels)

    # get times of movements
    active_times = file['time_sync'][file['index_mov']-1].reshape(-1)
    movements = len(active_times)

    # compute total number of spikes in active intervals
    spikenum = np.zeros(movements)
    for ch in channels:
        spikes = file[ch]
        for sp in spikes:
            for t in range(movements):
                if active_times[t] - interval/2 < sp[0] < active_times[t] + interval/2:
                    spikenum[t] += 1

    # average activity per channel
    rates = spikenum / (16 - lowchannel + 1) / interval

    active_mean = np.mean(rates)
    active_stddev = np.std(rates)
    active_median = np.median(rates)
    return active_mean, active_stddev, active_median, rates


# files to be processed
files = commons.all_healthy_cfa

# process files
baseline_medians = np.zeros(len(files))
for idx, f in enumerate(files):
    fname = os.path.join(commons.datapath, commons.healthy_cfa, f)
    baseline = find_baseline(fname, 12, 1.0)
    activity = find_active(fname, 12, 0.5)
    print("{}, baseline: mean={}, std={}, median={}; active: mean={}, std={}, median={}".format(f,
                                                                                                baseline[0],
                                                                                                baseline[1],
                                                                                                baseline[2],
                                                                                                activity[0],
                                                                                                activity[1],
                                                                                                activity[2]))
    baseline_medians[idx] = baseline[2] / 3

for idx, f in enumerate(files):
    print("{}: {}".format(f, baseline_medians[idx]))
