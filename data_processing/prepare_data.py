import os
import scipy.io as sio

import sys
sys.path.append("..")
import commons


def create_spike_file(infile, outfile, lowchannel, movementsonly=False, movementinterval=0.5):

    # load file
    channels = ['ts' + str(i) for i in range(lowchannel, 16 + 1)]
    file = sio.loadmat(infile, variable_names=['time_sync', 'index_mov'] + channels)

    # get times of movements
    active_times = file['time_sync'][file['index_mov']-1].reshape(-1)
    movements = len(active_times)

    # create file
    gid = 0
    with open(outfile, 'w') as outf:
        for ch in channels:
            spikes = file[ch]

            for sp in spikes:

                if movementsonly:
                    for t in range(movements):
                        if active_times[t] - movementinterval/2 < sp[0] < active_times[t] + movementinterval/2:
                            outf.write("{} {}\n".format(gid, sp[0]))
                else:
                    outf.write("{} {}\n".format(gid, sp[0]))
            gid += 1


def create_sled_file(infile, outfile):

    # load file
    file = sio.loadmat(infile, variable_names=['trial_sync', 'time_sync'])

    # there is a reset of the slide when trial_sync goes from 0 to 1
    with open(outfile, 'w') as outf:
        for i in range(len(file['trial_sync'])):
            if (file['trial_sync'][i] == 1) and (file['trial_sync'][i-1] == 0):
                outf.write("{}\n".format(file['time_sync'][i][0]))


# process files
trials = commons.all_healthy_rfa + commons.all_stroke_rfa

for i, t in enumerate(trials):

    # progress
    print("Processing file {}/{} ({})".format(i+1, len(trials), t.name))

    # spikes
    create_spike_file(t.name_full, t.prepared_spikes_full, 12)

    # spikes (movement only)
    create_spike_file(t.name_full, t.prepared_spikes_movements_full, 12, True)

    # slidetimes
    create_sled_file(t.name_full, t.prepared_slidetimes_full)
