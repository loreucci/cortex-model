## MATLAB code

To use the MATLAB scripts provided, you should include a file called `datapath.txt` into the matlab folder. This file should contain only the path to the folder containing the data (that is the folder containing 'Healthy-CFA' and the likes).


## Python code

### Dependencies
* python 3
* python dependencies listed in the `requirements.txt` file (can be installed with `pip install -r requirements.txt`)
* NEST > 2.18 (must be installed manually)

### Data folders
In addition to installing the dependencies, you should include a file called `datapath.py` into the commons folder. A template `datapath.py.sample` is provided.

### Data processing
The `data_processing` folders contains scripts that gather gather informations from the data (such as baseline activity) or process the neural and kinematic activity to create input files for the simulation.
