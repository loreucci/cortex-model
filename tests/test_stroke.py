import nest

import sys
sys.path.append("..")
from models.stroke import apply_stroke
from models.parameters import default_neuron_params_ex


if __name__ == "__main__":

    nest.set_verbosity('M_WARNING')

    n1 = nest.Create('iaf_psc_exp', 500, default_neuron_params_ex)
    n2 = nest.Create('iaf_psc_exp', 500, default_neuron_params_ex)

    nest.Connect(n1, n2, 'all_to_all')

    cg = nest.Create('dc_generator', 1, {'amplitude': 100000.0})
    nest.Connect(cg, n1)
    nest.Connect(cg, n2)

    sd = nest.Create('spike_detector', 1)
    nest.Connect(n1, sd)
    nest.Connect(n2, sd)

    nest.Simulate(1000.0)

    print("pre")
    print("spikes: {}".format(nest.GetStatus(sd, 'n_events')[0]))
    active = set(nest.GetStatus(sd, 'events')[0]['senders'])
    print("active: {}".format(len(active)))

    nest.SetStatus(sd, {'n_events': 0})
    apply_stroke(n1, n2, 0.5)
    nest.Simulate(1000.0)

    print("\npost")
    print("spikes: {}".format(nest.GetStatus(sd, 'n_events')[0]))
    active = set(nest.GetStatus(sd, 'events')[0]['senders'])
    print("active: {}".format(len(active)))
