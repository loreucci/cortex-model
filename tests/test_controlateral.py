import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append("..")
from simulations.cfa_simulation import cfa_simulation
from models.parameters import cortex_optimized_params


if __name__ == "__main__":

    T = 1000.

    cortex_params = cortex_optimized_params

    max_test = 10
    controlateral_rates = np.arange(1, max_test+1) * 1000.0
    p_rates = np.zeros(controlateral_rates.shape)
    fs_rates = np.zeros(controlateral_rates.shape)
    avg_rates = np.zeros(controlateral_rates.shape)

    for i, r in enumerate(controlateral_rates):
        cortex_params['controlateral_rate'] = r
        p, fs, avg = cfa_simulation(T, cortex_params, plot=False)
        p_rates[i] = p
        fs_rates[i] = fs
        avg_rates[i] = avg

    plt.figure()
    plt.plot(controlateral_rates, p_rates, label="P")
    plt.plot(controlateral_rates, fs_rates, label="FS")
    plt.plot(controlateral_rates, avg_rates, label="avg")
    plt.xlabel("Controlateral rate (sp/s)")
    plt.ylabel("Rate (sp/s)")
    plt.legend()
    plt.show()
