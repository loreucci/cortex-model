import numpy as np
import matplotlib.pyplot as plt
import argparse
import nest
from nest import raster_plot

import sys
sys.path.append("..")
from simulations.cfa_simulation import cfa_simulation
from models.cortex import cfa
from commons.nest_utils import create_detector
from models.stroke import get_stroke_order, apply_stroke


def incremental_stroke():
    # simulation setup
    nest.ResetKernel()
    nest.SetKernelStatus({'resolution': 0.1, "local_num_threads": 4})  # set simulation resolution"

    cfa_p, cfa_fs = cfa()

    sd = create_detector([cfa_p, cfa_fs])

    nest.set_verbosity('M_ERROR')

    # remove transient
    nest.Simulate(10.0)
    nest.SetStatus(sd, {'n_events': 0})

    # 0% stroke
    nest.Simulate(100.0)

    stroke_order = get_stroke_order(cfa_p + cfa_fs)
    for p in np.arange(0.0, 1.01, step=0.01):
        print("Simulating stroke at {}%".format(p*100))
        apply_stroke(cfa_p, cfa_fs, p, stroke_order)
        nest.Simulate(100.0)

    raster_plot.from_device(sd)
    raster_plot.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser("test CFA stroke")
    parser.add_argument('--incremental', action="store_true")

    args = parser.parse_args()
    if args.incremental:
        incremental_stroke()
        quit(0)

    stroke_percentages = np.linspace(0.05, 1.0, 20)

    p_rates = np.zeros(stroke_percentages.shape)
    fs_rates = np.zeros(stroke_percentages.shape)
    avg_rates = np.zeros(stroke_percentages.shape)

    for i, perc in enumerate(stroke_percentages):
        _, _, _, rp, rfs, ravg = cfa_simulation(1000.0, stroke=perc, plot=False)
        p_rates[i] = rp
        fs_rates[i] = rfs
        avg_rates[i] = ravg

    plt.figure()
    plt.plot(stroke_percentages, p_rates, label="P")
    plt.plot(stroke_percentages, fs_rates, label="FS")
    plt.plot(stroke_percentages, avg_rates, label="avg")
    plt.xlabel("Stroke (%)")
    plt.ylabel("Rate (sp/s)")
    plt.legend()
    plt.show()
