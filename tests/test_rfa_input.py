import nest
from nest import raster_plot
import argparse
from nest_networks.spikes import import_spikes_from_file, scale_times, trim_to_resolution, get_gids, remap_gids, \
                                 get_spikes_from_detector, save_spikes_to_file
from nest_networks.recordingplayer import RecordingPlayer


import sys
sys.path.append("..")
from commons import all_healthy_rfa
from commons.nest_utils import create_detector
from models.parameters import cortex_optimized_params
from models import cortex


if __name__ == '__main__':

    parser = argparse.ArgumentParser("test RFA inputs")
    parser.add_argument('--fileid', type=int, default=1)
    parser.add_argument('--startcut', type=float, default=125.0)
    parser.add_argument('--simtime', type=float, default=1000.0)

    args = parser.parse_args()

    # input and parameters
    input_file = all_healthy_rfa[args.fileid]
    cfa_neurons = cortex_optimized_params["cfa_p_neurons"] + cortex_optimized_params["cfa_fs_neurons"]

    # RFA
    spikes_mu_tocut = import_spikes_from_file(input_file.prepared_spikes_full, 1)
    spikes_mu = [(idx, time - args.startcut) for idx, time in spikes_mu_tocut if time > args.startcut]
    spikes_mu = scale_times(spikes_mu, 1000.0)
    spikes_mu = trim_to_resolution(spikes_mu)
    gids = get_gids(spikes_mu)
    spikes_mu = remap_gids(spikes_mu, orig=gids, dest=range(len(gids)))
    rfa = RecordingPlayer(spikes_mu, mult=int(cfa_neurons / 2.0) // 40, sigma=5.)

    # CFA
    cfa_p, cfa_fs = cortex.cfa()

    # RFA to CFA
    nest.Connect(rfa.get_gids(),
                 cfa_p[0:int(len(cfa_p) * 0.1)],
                 'all_to_all',
                 # {'rule': 'fixed_total_number', 'N': int(0.1*cfa_p_neurons*len(rfa.get_gids()))},
                 {'weight': cortex_optimized_params['w_p_p'], 'delay': 10.})

    # spike detectors
    sd = create_detector([cfa_p, cfa_fs, rfa.get_gids()])

    # remove transient
    nest.Simulate(10.0)
    nest.SetStatus(sd, {'n_events': 0})

    # simulation
    nest.Simulate(args.simtime)

    # plot results
    raster_plot.from_device(sd)
    raster_plot.show()
